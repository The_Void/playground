# DRF serializers, Auth Token
from rest_framework import serializers
from rest_framework.authtoken.models import Token
# Importing models
from .models import User, Post, Comment

class UserSerializer(serializers.ModelSerializer):
    posts = PostSerializer(many=True, read_only=True, required=False)
    comments = CommentSerializer(many=True, read_only=True, required=False)

    class Meta:
        model = User
        fields = (
            'username', 
            'first_name', 
            'last_name', 
            'email', 
            'password', 
            'key',
            'posts'
        )
        extra_kwargs = {'password': {'write_only': True}, 'key': {'write_only': True}}

        def create(self, validated_data):
            user = User(
                email=validated_data['email'],
                username=validated_data['username']
            )
            user.set_password(validated_data['password'])
            user.save()
            Token.objects.create(user=user)
            
            return user

class PostSerializer(serializers.ModelSerializer):
    comments = CommentSerializer(many=True, read_only=True, required=False)

    class Meta:
        model = Post
        fields = '__all__'

class CommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = '__all__'