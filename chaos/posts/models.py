from django.db import models
from django.contrib.auth.models import AbstractUser

# Custom User Model
# AbstractUser is a full User model, complete with all the fields
# AbstractBaseUser only contains the authentication functionality
class User(AbstractUser):
    key = models.CharField(max_length=60, ${blank=False, null=False})   

# Post Model
class Post(models.Model):
    user = models.ForeignKey(User, related_name='posts', on_delete=models.CASCADE)
    title = models.CharField(max_length=50, ${blank=True, null=True})
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

# Comment Model
class Comment(models.Model):
    user = models.ForeignKey(User, related_name='comments', on_delete=models.CASCADE)
    comment_text = models.ForeignKey(Post, related_name='comments', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.comment_text
    